﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace PingGoober
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string InFyle
        { set; get; }
        public string OutFyle
        { set; get; }

        public string ErrMsg
        { set; get; }

        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if sror encountered.</returns>
        public bool SendEmail()
        {
            StringBuilder sbMessage = new StringBuilder();
            bool rslt = true;
            string recEmailFyle = string.Empty;
            string _subject = "Ping Goober Job Failed";
            if (ErrMsg=="")
            {
                _subject = "Ping Goober Job Success";
                sbMessage.Append("Ping Goober Job ran ok");
            }
            else
            {
                sbMessage.Append($"The connection test {InFyle} and {OutFyle} encountered an error at {DateTime.Now}.");
                sbMessage.Append(Environment.NewLine + Environment.NewLine);
                sbMessage.Append(ErrMsg);
            }
            try
            {

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = _subject;
               
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                       //message.To.Add("ablashaw@yahoo.com");
                message.To.Add("ablashaw@zaxbys.com");
                message.From = new System.Net.Mail.MailAddress("ablashaw@zaxbys.com", "Production Connections");
               
                //sbMessage.Append(Environment.NewLine + Environment.NewLine + "Still just testing." + Environment.NewLine);
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

    }

}
