﻿using System;
using System.Net.NetworkInformation;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace PingGoober
{
    class Program
    {
        static void Main(string[] args)
        {
            string ipAddr = "10.252.192.29";
            Ping pinger = new Ping();
            int cnt = 0;
            bool sendEmail = false;
            bool _found = false;
            string _result = "";
            string _errmsg = "";
            string _sendMailOnSuccess = "yes";

            // check when the ping is not success
            try
            {
                PingReply pingRply = pinger.Send(ipAddr);
                _result = pingRply.Status.ToString();
                if(_result != "Success")
                {
                    
                    _errmsg = "Goober Ping Job:  Attempted ping reurned the following result: " + _result;

                }


            }
            catch (Exception ex)
            {
               
                _errmsg = "Goober Ping Job:  " + ex.Message.ToString();
            }


            //while (!pingRply.Status.ToString().Equals("Success"))
            //{
                //Console.WriteLine(pingRply.Status.ToString());
              //  pingRply = pinger.Send(ipAddr);
            //}
            // check after the ping is n success

            if (_errmsg=="")
            {
                try
                {

                    System.Diagnostics.Process objProcess = new System.Diagnostics.Process();
                    objProcess.StartInfo.FileName = "connect.bat";
                    objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;    // to hide the command window popping up
                    objProcess.Start();
                    objProcess.WaitForExit();    // Gives time for the process to complete operation.
                                                 // After code is executed, call the dispose() method
                    objProcess.Dispose();

                    string[] theFolders = Directory.GetDirectories(@"\\goober\ftpusers");
                }
                catch(Exception ex)
                {
                    _errmsg = "Goober Ping Job:  " + ex.Message.ToString();
                }


               
            }

            String strHostName = string.Empty;
            // Getting Ip address of local machine...
            // First get the host name of local machine.
            strHostName = Dns.GetHostName();
            // Then using host name, get the IP address list..
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;



            if (_errmsg!="")
            {
               
                using (CodeEmail clsEmail = new CodeEmail())
                {
                    clsEmail.ErrMsg = _errmsg;
                    clsEmail.InFyle = addr[1].ToString();
                    clsEmail.OutFyle = ipAddr;
                    clsEmail.SendEmail();
                }
            }
            else if (_sendMailOnSuccess=="yes")
            {
                using (CodeEmail clsEmail = new CodeEmail())
                {
                    clsEmail.ErrMsg = "";
                    clsEmail.InFyle = addr[1].ToString();
                    clsEmail.OutFyle = ipAddr;
                    clsEmail.SendEmail();
                }
            }

        }
    }
}
